@extends('dashboard.layouts.app')
@section('content')
    <!--start content-->
    <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
            <div class="breadcrumb-title pe-3">Product</div>
            <div class="ps-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Product</li>
                    </ol>
                </nav>
            </div>
          
        </div>
        <!--end breadcrumb-->

<div class="row">



    <div class="card">
      <div class="card-body">
        <div class="border p-3 rounded">
        <h6 class="mb-0 text-uppercase">Edit Product</h6>
         <hr/>
         @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <form class="row g-3" action="{{route('update.product',$product->id)}}" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="col-6">
           <img id="preview" src="{{asset('storage/'.$product->image)}}" style="height: 280px;
           width: 280px;
           object-fit: contain;" >
            <input type="file" onchange="document.getElementById('preview').src = window.URL.createObjectURL(this.files[0])" name="image" class="form-control">

          </div>
          <div class="col-6">
            <label class="form-label">Name</label>
            <input type="text" name="name" value="{{$product->name}}" class="form-control">
            <label class="form-label">Price</label>
            <input type="number" name="price" value="{{$product->price}}" class="form-control">
            <label class="form-label">Description</label>
            <textarea rows="5" name="description" placeholder="write text here ...." class="form-control">{!!$product->detail !!} </textarea>
          </div>
        
       
        
          <div class="col-12">
            <div class="d-grid">
              <button type="submit" class="btn btn-primary">Update </button>
            </div>
          </div>
       
        </form>
      </div>
      </div>
    </div>

            </div>
    

    </main>
<!--end page main-->  
@endsection