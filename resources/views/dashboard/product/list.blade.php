@extends('dashboard.layouts.app')
@section('content')
      <!--start content-->
      <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
          <div class="breadcrumb-title pe-3">Product</div>
          <div class="ps-3">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb mb-0 p-0">
                <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">All Product</li>
              </ol>
            </nav>
          </div>
        </div>
        <!--end breadcrumb-->

           <div class="card">
             <div class="card-body">
               <div class="d-flex align-items-center">
                  <h5 class="mb-0">Products ({{$count}})</h5>
                   <form class="ms-auto position-relative">
                     <div class="position-absolute top-50 translate-middle-y search-icon px-3"><i class="bi bi-search"></i></div>
                     <input class="form-control ps-5" type="text" placeholder="search">
                   </form>
               </div>
               <div class="table-responsive mt-3">
                 <table class="table align-middle">
                   <thead class="table-secondary">
                     <tr>
                     
                      <th>Id</th>
                      <th>name</th>
                      <th>Price</th>
                      <th>description</th>
                     <th>Actions</th>
                     </tr>
                   </thead>
                   <tbody>
                    @foreach ($products as $product)
                        
                    <tr>
                      <td>{{$product->id}}</td>
                       <td>
                         <div class="d-flex align-items-center gap-3 cursor-pointer">
                            <img src="{{asset('storage/'.$product->image)}}" class="rounded-circle" width="44" height="44" alt="">
                            <div class="">
                              <p class="mb-0">{{$product->name}}</p>
                            </div>
                         </div>
                       </td>
                      
                       <td>{{$product->price}}</td>
                       <td>{{$product->detail}}</td>
                     
                       <td>
                         <div class="table-actions d-flex align-items-center gap-3 fs-6">
                          
                           <a href="{{route('edit.product',$product->id)}}" class="text-warning" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit"><i class="bi bi-pencil-fill"></i>edit</a>
                          @if($product->hide== true)
                          <a href="{{route('status.product',$product->id)}}" class="text-warning" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Show"><i class="bi bi-eye"></i>Show</a>
                          @else
                           <a href="{{route('status.product',$product->id)}}" class="text-danger" data-bs-toggle="tooltip" data-bs-placement="bottom" title="hide"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye-off"><path d="M17.94 17.94A10.07 10.07 0 0 1 12 20c-7 0-11-8-11-8a18.45 18.45 0 0 1 5.06-5.94M9.9 4.24A9.12 9.12 0 0 1 12 4c7 0 11 8 11 8a18.5 18.5 0 0 1-2.16 3.19m-6.72-1.07a3 3 0 1 1-4.24-4.24"></path><line x1="1" y1="1" x2="23" y2="23"></line></svg></a>
                            @endif
                        </div>
                       </td>
                     </tr>
                     @endforeach
                   </tbody>
                 </table>
               </div>
             </div>
           </div>

      </main>
   <!--end page main-->

@endsection