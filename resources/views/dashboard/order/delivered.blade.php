@extends('dashboard.layouts.app')
@section('content')
      <!--start content-->
      <main class="page-content">
        <!--breadcrumb-->
        <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
          <div class="breadcrumb-title pe-3">Orders</div>
          <div class="ps-3">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb mb-0 p-0">
                <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">Delivered Order</li>
              </ol>
            </nav>
          </div>
        </div>
        <!--end breadcrumb-->

           <div class="card">
             <div class="card-body">
               <div class="d-flex align-items-center">
                  <h5 class="mb-0">Delivered ({{count($orders)}})</h5>
                   <form class="ms-auto position-relative">
                     <div class="position-absolute top-50 translate-middle-y search-icon px-3"><i class="bi bi-search"></i></div>
                     <input class="form-control ps-5" type="text" placeholder="search">
                   </form>
               </div>
               <div class="table-responsive mt-3">
                 <table class="table align-middle">
                   <thead class="table-secondary">
                     <tr>
                     
                      <th>name</th>
                      <th>Mobile</th>
                      <th>Address</th>
                      <th>Products</th>
                   
                     </tr>
                   </thead>
                   <tbody>
                     @foreach($orders as $order)
                    <tr>
                        <td>{{$order->client->name}}</td>
                        <td>{{$order->client->mobile}}</td>
                        <td>{{$order->client->address}}</td>
                       
                         <td>
                      @php
                       $amount = 0   
                      @endphp   
                     @foreach($order->detail as $detail)
                      @php
                      $amount = $amount + $detail->product->price
                      @endphp
                     <span class="badge bg-secondary">{{$detail->product->name}}</span>
                            
                            <br>

                        @endforeach
                        <hr>
                       {{ $amount}}
                            </td>
                         
                     </tr>
                     @endforeach
                   </tbody>
                 </table>
               </div>
             </div>
           </div>

      </main>
   <!--end page main-->

@endsection