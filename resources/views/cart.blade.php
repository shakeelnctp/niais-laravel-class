@extends('index.app')
@section('content')
  <!-- Page Banner Section Start -->
  <div class="page-banner-section section" style="background-image: url(assets/images/hero/hero-1.jpg)">
    <div class="container">
        <div class="row">
            <div class="page-banner-content col">

                <h1>Cart</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li><a>Cart</a></li>
                </ul>

            </div>
        </div>
    </div>
</div><!-- Page Banner Section End -->

<!-- Page Section Start -->
<div class="page-section section section-padding">
    <div class="container">

        <form action="{{route('place.order')}}" method="POST">
            @csrf				
            <div class="row mbn-40">
                <div class="col-12 mb-40">
                    <div class="cart-table table-responsive">
                        <table>
                            <thead>
                                <tr>
                                    <th class="pro-thumbnail">Image</th>
                                    <th class="pro-title">Product</th>
                                    <th class="pro-price">Price</th>
                                    <th class="pro-quantity">Quantity</th>
                                   
                                    <th class="pro-remove">Remove</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $amount = 0
                                @endphp
                               @foreach($cart as $cart)
                                <tr>
                                    <td class="pro-thumbnail"><a href="#"><img src="{{asset('storage/'.$cart->product->image)}}" alt="" /></a></td>
                                    <td class="pro-title"><a href="#">{{$cart->product->name}}</a></td>
                                    <td class="pro-price"><span class="amount">{{$cart->product->price}}</span></td>
                                    <td class="pro-quantity">1</td>
                                  
                                    <td class="pro-remove"><a href="{{route('remove.cart',$cart->id)}}">×</a></td>
                                </tr>
                                @php
                                $amount = $amount + $cart->product->price
                            @endphp
                               @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
              
                <div class="col-lg-8 col-md-7 col-12 mb-40">
                    <div class=" mb-30">
                        <input name="mobile" value="{{$user->mobile}}" required placeholder="enter your mobile"/>
                        <input name="address" value="{{$user->address}}" required placeholder="enter your address"/>

                    </div>
                   
                </div>
                <div class="col-lg-4 col-md-5 col-12 mb-40">
                    <div class="cart-total fix">
                        <h3>Cart Totals</h3>
                        <table>
                            <tbody>
                              
                                <tr class="order-total">
                                    <th>Total</th>
                                    <td>
                                        <strong><span class="amount">{{$amount}}</span></strong>
                                    </td>
                                </tr>											
                            </tbody>
                        </table>
                        <div class="cart-buttons section mt-30">
                            <button type="submit">Proceed to Checkout</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div><!-- Page Section End -->

<!-- Brand Section Start -->
<div class="brand-section section section-padding pt-0">
    <div class="container-fluid">
        <div class="row">
            <div class="brand-slider">

                <div class="brand-item col">
                    <img src="assets/images/brands/brand-1.png" alt="">
                </div>

                <div class="brand-item col">
                    <img src="assets/images/brands/brand-2.png" alt="">
                </div>

                <div class="brand-item col">
                    <img src="assets/images/brands/brand-3.png" alt="">
                </div>

                <div class="brand-item col">
                    <img src="assets/images/brands/brand-4.png" alt="">
                </div>

                <div class="brand-item col">
                    <img src="assets/images/brands/brand-5.png" alt="">
                </div>

                <div class="brand-item col">
                    <img src="assets/images/brands/brand-6.png" alt="">
                </div>

            </div>
        </div>
    </div>
</div><!-- Brand Section End -->
@endsection