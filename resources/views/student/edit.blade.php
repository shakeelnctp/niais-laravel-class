<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Update Student Profile</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('style.css')}}"/>
</head>
<body>
    <div class="row">
        <div class="col-8 my-style">
            <h1>Update Student Profile</h1>
            @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
            <hr>
            <form action="{{route('update',$student->id)}}" method="POST">
                @csrf
                <div class="form-group">
                  <label >Name</label>
                  <input type="text" class="form-control" value="{{$student->name}}" name="name" placeholder="Enter Student Name">
                
                </div>
                <div class="form-group">
                    <label >Email address</label>
                    <input type="email" name="email" value="{{$student->email}}" class="form-control"placeholder="Enter email">
                  
                  </div>
                  <div class="form-group">
                    <label >Mobile</label>
                    <input type="text" name="mobile" value="{{$student->mobile}}" class="form-control"placeholder="Enter Student Mobile Number">
                  
                  </div>
                  <div class="form-group">
                    <label >CNIC</label>
                    <input type="text" name="cnic" value="{{$student->cnic}}" class="form-control"placeholder="Enter Student CNIC">
                  
                  </div>
                  <div class="form-group">
                    <label >Address</label>
                   <textarea class="form-control" name="address" placeholder="Enter Address Here ..." rows="4">{!! $student->address !!}</textarea>
                  </div>
              
                <button type="submit" class="btn btn-primary">Update</button>
              </form>
        </div>
    </div>
    
</body>
</html>