<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create Student Profile</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('style.css')}}"/>
</head>
<body>
    <div class="row">
        <div class="col-8 my-style">
            <h1>List Student Profile</h1>
            <a href="{{route('create')}}" class="btn btn-success">+ Add New</a>
            <hr>

            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Mobile</th>
                    <th scope="col">CNIC</th>
                    <th scope="col">Address</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                 @foreach ($students as $student)
                  <tr>
                    <th scope="row">{{$student->id}}</th>
                    <td>{{$student->name}}</td>
                    <td>{{$student->email}}</td>
                    <td>{{$student->mobile}}</td>
                    <td>{{$student->cnic}}</td>
                    <td>{{$student->address}}</td>
                    <td>
                      <a href="{{route('edit',$student->id)}}" class="btn btn-warning">Edit</a>
                    <a href="{{route('delete',$student->id)}}" class="btn btn-danger">Delete</a>
                    </td>
             
                  </tr>
                  @endforeach
                </tbody>
              </table>
                   </div>
    </div>
    
</body>
</html>