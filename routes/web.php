<?php

use App\Http\Controllers\IndexController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\StudentController;
use App\Models\Ecommerce\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/app', function () {
    return view('dashboard.layouts.app');
});
Route::get('/',[IndexController::class,'index'])->name('index');
Route::get('/product-detail/{id?}', [IndexController::class,'detail']);
Route::get('portfolio/{id?}',[IndexController::class ,'portfolio']);
Route::get('cart',[IndexController::class ,'cart'])->name('cart')->middleware('auth');
Route::get('add-to-cart/{id?}',[IndexController::class ,'addToCart'])->name('add.to.cart')->middleware('auth');
Route::get('remove-cart/{id?}',[IndexController::class ,'removeCart'])->name('remove.cart')->middleware('auth');
Route::post('place-order',[IndexController::class ,'placeOrder'])->name('place.order')->middleware('auth');

Auth::routes();

Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth','admin');
Route::get('/home-2', [App\Http\Controllers\HomeController::class, 'index2'])->name('home2');


///*********************** PRODUCT ROUTES ********************************/
Route::middleware(['auth','admin'])->group(function () {
    
    /////Store Product
    Route::get('create-product',[ProductController::class,'create'])->name('create.product');
    Route::post('store-product',[ProductController::class,'store'])->name('store.product');
   
    /////// List Product
    Route::get('list-product',[ProductController::class,'list'])->name('list.product');

    //Action Product
    Route::get('status-product/{id}',[ProductController::class,'updateStatus'])->name('status.product');
    Route::get('edit-product/{id}',[ProductController::class,'editProduct'])->name('edit.product');
    Route::post('update-product/{id}',[ProductController::class,'updateProduct'])->name('update.product');

    //Orders Routes//////
    Route::get('orders/{id}',[OrderController::class,'orders'])->name('orders');
    Route::get('change-status/{orderid?}/{statusid?}',[OrderController::class,'changeStatus'])->name('change.status');

});

///*********************** CRUD ROUTES ********************************/

//Create
Route::get('create-profile',[StudentController::class,'create'])->name('create');
//Store
Route::post('store-profile',[StudentController::class,'store'])->name('store');
//List
Route::get('list-profile',[StudentController::class,'show'])->name('list');
//Delete
Route::get('remove-profile/{id}',[StudentController::class,'delete'])->name('delete');
//Edit
Route::get('edit-profile/{id}',[StudentController::class,'edit'])->name('edit');
//Update
Route::post('update-profile/{id}',[StudentController::class,'update'])->name('update');