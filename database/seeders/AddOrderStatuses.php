<?php

namespace Database\Seeders;

use App\Models\Ecommerce\OrderStatus;
use Illuminate\Database\Seeder;

class AddOrderStatuses extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /////1
        OrderStatus::create([
            'name' => 'New'
        ]);
         /////2
         OrderStatus::create([
            'name' => 'Accepted'
        ]);
         /////3
         OrderStatus::create([
            'name' => 'Rejected'
        ]);
         /////4
         OrderStatus::create([
            'name' => 'Delivered'
        ]);
    }
}
