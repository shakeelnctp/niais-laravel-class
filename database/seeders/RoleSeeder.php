<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ////1////
        Role::create([
            'name' => 'Admin'
        ]);

         ////2////
         Role::create([
            'name' => 'Client'
        ]);
    }
}
