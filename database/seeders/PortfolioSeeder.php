<?php

namespace Database\Seeders;

use App\Models\Portfolio;
use Illuminate\Database\Seeder;

class PortfolioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ///1
        Portfolio::create([
            'name' => 'Shakeel',
            'cnic' => "1242234234234",
            'mobile' => '77565656'
        ]);


        //2
        Portfolio::create([
            'name' => 'Rashid',
            'cnic' => "1242234234234",
            'mobile' => '77565656'
        ]);     
        //3
        Portfolio::create([
            'name' => 'Ghalid Butt',
            'cnic' => "1242234234234",
            'mobile' => '77565656'
        ]);
    }
}
