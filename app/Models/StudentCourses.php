<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentCourses extends Model
{
    
    protected $hidden = [
        'id',
    ];


    use HasFactory;


    public function student()
    {
        return $this->belongsTo(Portfolio::class,'student_id','id');
    }
}
