<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
 use App\Models\StudentCourses;
class Portfolio extends Model
{
    use HasFactory;



    //   public function courses()
    //   {
    //      return $this->hasMany(StudentCourses::class,'student_id','id'); 
    //   }

      public function courses()
      {
         return $this->hasOne(StudentCourses::class,'student_id','id'); 
      }
}
