<?php

namespace App\Models\Ecommerce;

use App\Models\OrderDetail;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;


    protected $guarded = [];

    public function status()
    {
        return $this->belongsTo(OrderStatus::class,'order_status_id','id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }

    public function client()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function detail()
    {
        return $this->hasMany(OrderDetail::class,'order_id','id');
    }
}
