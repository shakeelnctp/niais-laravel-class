<?php

namespace App\Models;

use App\Models\Ecommerce\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Cart extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function product()
    {
        return  $this->BelongsTo(Product::class,'product_id','id');
    }
}
