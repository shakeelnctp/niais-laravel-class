<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Ecommerce\Order;
use App\Models\Ecommerce\Product;
use App\Models\OrderDetail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{



  public function index()
  {

    $products  =  Product::where('hide', false)->get();
    //  dd($products[0]->name);
    return view('welcome', compact('products'));
  }

  public function detail($id = 1)
  {
    $product = Product::find($id);
    //  dd($product);
    return view('product.detail', compact('product'));
  }

  public function portfolio($code)
  {

    // dd($code);

    if ($code == "test0001") {
      return view('portfolio');
    } else {
      dd('invalid code');
    }
  }



  public function cart()
  {
    $user = Auth::user();
    $cart = $user->cart;

    return view('cart', compact('cart', 'user'));
  }

  public function addToCart($id)
  {
    $user = Auth::user();

    $stauts =   Cart::where('user_id', $user->id)->where('product_id', $id)->first();
    if (!$stauts) {
      Cart::create([
        'user_id' => $user->id,
        'product_id' => $id,
      ]);

      return back();
    }

    return back();
  }

  public function removeCart($id)
  {
    cart::find($id)->delete();
    return back();
  }

  public function placeOrder(Request $request)
  {
    $user = User::find(Auth::user()->id);

    $request->validate([
      'mobile' => "required",
      'address' => "required",
    ]);
    $user->mobile = $request->mobile;
    $user->address  = $request->address;
    $user->save();

    $cart = $user->cart;


    if ($cart->all()) {
      
      $order = Order::create([
        'user_id' => $user->id,
        'order_status_id' => 1,
      ]);

      foreach ($cart as  $cart) {
        OrderDetail::create([
          'order_id' => $order->id,
          'product_id' => $cart->product_id
        ]);

        Cart::find($cart->id)->delete();
      }
    }


    return redirect()->route('index');
  }
}
