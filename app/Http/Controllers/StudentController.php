<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function create()
    {
        return view('student.create');
    }

    public function store(Request $request)
    {
    //    dd($request->all()); 

       $request->validate([
        "name" => "required",
        "email" => "required|unique:students",
        "mobile" => "required|unique:students",
        "cnic" => "required|unique:students",
        "address" => "required"
       ]);

    //    Student::create($request->all());

       Student::create([
        "name" => $request->name,
        "email" => $request->email,
        "mobile" => $request->mobile,
        "cnic" => $request->cnic,
        "address" => $request->address
       ]);

       return redirect()->route('list');
       
       
       //    return back()->withErrors('Successfuly Submitted');

    }

    public function show()
    {
        $students =  Student::all();
        return view('student.list',compact('students'));
    }


    public function delete($id)
    {
        Student::find($id)->delete();
        return back();
    }

    public function edit($id)
    {
       $student =  Student::find($id);
       return view('student.edit',compact('student'));
    }


    public function update(Request $request, $id)
    {
    //    dd($request->all()); 

       $request->validate([
        "name" => "required",
        "email" => "required",
        "mobile" => "required",
        "cnic" => "required",
        "address" => "required"
       ]);

    //   Student::find($id)->update($request->all());

      $student =  Student::find($id);
      $student->name  = $request->name;
      $student->email = $request->email;
      $student->mobile = $request->mobile;
      $student->cnic = $request->cnic;
      $student->address = $request->address;
      $student->save();

       return redirect()->route('list');
       
       
       //    return back()->withErrors('Successfuly Updated');

    }
}
