<?php

namespace App\Http\Controllers;

use App\Models\Ecommerce\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function create()
    {
        return view('dashboard.product.create');
    }
    public function store(Request $request)
    {

        // dd($request->all());
       $request->validate([
           'name' => "required",
           'price' => "required",
           'description' => "required",
           'image' => "required|mimes:jpg,jpeg,png",
       ]);

       if ($request->hasFile('image')) {
        $path = $request->file('image');
        $target = 'public/product';
        $file = Storage::putFile($target, $path);
        $file = substr($file, 7, strlen($file) - 7);
    }
       Product::create([
           'name' => $request->name,
           'price' => $request->price,
           'detail' => $request->description,
           'image'  => $file
       ]);

       return redirect()->route('list.product');

    }

    public function list()
    {
       $products =  Product::all();
       $count =  Product::count();
       
        return view('dashboard.product.list',compact('products','count'));
    }
    public function updateStatus($id)
    {
        $product =  Product::find($id);

        if($product->hide == true)
        {
            $product->hide = false ;            
        }else{
            $product->hide = true ;            
        }

        $product->save();
        return back();  
    }

    public function editProduct($id)

    {
        $product = Product::find($id);

        return view('dashboard.product.edit',compact('product'));
    }

    public function updateProduct(Request $request , $id)

    {

        $request->validate([
            'name' => "required",
            'price' => "required",
            'description' => "required",
        ]);
 
        $file = null;
        if ($request->hasFile('image')) {
         $path = $request->file('image');
         $target = 'public/product';
         $file = Storage::putFile($target, $path);
         $file = substr($file, 7, strlen($file) - 7);
     }
        $product = Product::find($id);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->detail = $request->description;
         if($file){
             $product->image = $file;
         }
         $product->save();

         
        return redirect()->route('list.product');
       
    }

}
