<?php

namespace App\Http\Controllers;

use App\Models\Ecommerce\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function orders($id)
    {
        if($id == 1){

            $orders = Order::where('order_status_id',$id)->get();

            return view('dashboard.order.new',compact('orders'));

        }elseif($id == 2){

            $orders = Order::where('order_status_id',$id)->get();
            return view('dashboard.order.approved',compact('orders'));


        }elseif($id == 3){
            
            $orders = Order::where('order_status_id',$id)->get();
            return view('dashboard.order.rejected',compact('orders'));


        }elseif($id == 4){
            $orders = Order::where('order_status_id',$id)->get();
            return view('dashboard.order.delivered',compact('orders'));

        }
        elseif($id == 5){
            $orders = Order::orderBy('id','DESC')->get();
            return view('dashboard.order.all',compact('orders'));
 
        }
    }

    public function changeStatus(Request $request)

    {
       $order = Order::find($request->orderid)->update(['order_status_id'=> $request->statusid]);
        return back();
    }
}
